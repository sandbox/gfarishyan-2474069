<?php
/**
 * @file
 * Defines nodeview stats main functionality.
 */

define('NODEVIEW_STATS_ENABLED', 1);
define('NODEVIEW_STATS_DISABLED', 0);

/**
 * Implements hook_form_FORM_ID_alter().
 */
function nodeview_stats_form_node_type_form_alter(&$form, $form_state) {
  if (isset($form['type'])) {
    $form['nodeview_stats'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node view statistics'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('nodeview-stats-node-type-settings-form'),
      ),
    );

    $form['nodeview_stats']['nodeview_stats_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Stats'),
      '#default_value' => variable_get('nodeview_stats_enabled_' . $form['#node_type']->type, NODEVIEW_STATS_DISABLED),
    );
    $form['#submit'][] = 'nodeview_stats_node_type_form_submit';
  }
}

/**
 * Implements hook_node_type_form_submit().
 */
function nodeview_stats_node_type_form_submit($form, &$form_state) {
  variable_set('nodeview_stats_enabled_' . $form_state['values']['type'], $form_state['values']['nodeview_stats_enabled']);
}

/**
 * Implements hook_node_prepare().
 */
function nodeview_stats_node_prepare($node) {
  if (isset($node->nid)) {
    $node_stats_info = _nodeview_stats_get_node_info($node);
    if (!empty($node_stats_info)) {
      $node->nodeview_stats_enabled = $node_stats_info['status'];
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function nodeview_stats_form_node_form_alter(&$form, $form_state) {
  $node = $form_state['node'];
  // Node options for administrators.
  $form['nodeview_stats'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('administer nodes'),
    '#title' => t('Node view statistics'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('node-form-nodeview-stats'),
    ),
    '#weight' => 95,
  );

  $node_stats_info = _nodeview_stats_get_node_info($node);
  $nv_stats_enabled = NULL;

  if (!empty($node_stats_info)) {
    $nv_stats_enabled = $node_stats_info['status'];
  }
  else {
    $nv_stats_enabled = variable_get('nodeview_stats_enabled_' . $node->type, NODEVIEW_STATS_DISABLED);
  }

  $form['nodeview_stats']['nodeview_stats_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable stats'),
    '#default_value' => $nv_stats_enabled,
  );
}

/**
 * Implements hook_node_type_delete().
 */
function nodeview_stats_node_type_delete($info) {
  variable_del('nodeview_stats_enabled_' . $info->type);
}

/**
 * Implements hook_node_insert().
 */
function nodeview_stats_node_insert($node) {
  if (!isset($node->nodeview_stats_enabled)) {
    return;
  }

  db_insert('nodeview_stats_nodes')
    ->fields(array(
      'nid' => $node->nid,
      'status' => $node->nodeview_stats_enabled,
      'view_mode' => serialize(array('full')),
    ))
    ->execute();
}

/**
 * Implements hook_node_update().
 */
function nodeview_stats_node_update($node) {
  if (!isset($node->nodeview_stats_enabled)) {
    return;
  }

  $node_info = _nodeview_stats_get_node_info($node);
  $fields = array(
    'status' => $node->nodeview_stats_enabled,
    'view_mode' => serialize(array('full')),
  );

  if (!empty($node_info)) {
    db_update('nodeview_stats_nodes')
      ->fields($fields)
      ->condition('nid', $node->nid, '=')
      ->execute();
  }
  else {
    db_insert('nodeview_stats_nodes')
      ->fields($fields)
      ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function nodeview_stats_node_delete($node) {
  db_delete('nodeview_stats_nodes')->condition('nid', $node->nid)->execute();
  db_delete('nodeview_stats')->condition('nid', $node->nid)->execute();
}

/**
 * Implements hook_node_view().
 */
function nodeview_stats_node_view($node, $view_mode, $langcode) {
  $info = _nodeview_stats_get_node_info($node);

  if (!empty($info) && $info['status']) {
    if (in_array($view_mode, $info['view_mode'])) {
      global $user;
      $sid = '';
      $uid = $user->uid;
      $sid = session_id();

      $record = array(
        'nid' => $node->nid,
        'view_mode' => $view_mode,
        'created' => time(),
        'referer' => $_SERVER['HTTP_REFERER'],
        'uid' => $uid,
        'sid' => $sid,
    );

	  $result = db_select('sessions', 'n')
	  ->fields('n', array('timestamp'))
	  ->condition('sid', $sid, '=')
		->execute()
		->fetchCol();	
		
foreach ($result as $res){
	$r[$res] = $res;
}
	$node_res = db_select('nodeview_stats', 'm')
	->fields('m', array('sid'))
	->execute()
	->fetchCol();
			
	
	$node_res1 = db_select('nodeview_stats', 'm')
	->fields('m', array('sid'))
	->condition('nid', $node->nid, '=')
	->execute()
	->fetchCol();
	
	$node_res2 = db_select('nodeview_stats', 'm')
	->fields('m', array('sid'))
	->condition('nid', $node->nid, '<>')
	->execute()
	->fetchCol();

if (!in_array($uid, variable_get('nodeview_not_tracking_users'))){
	  if (!in_array($sid, $node_res)){
	  drupal_write_record('nodeview_stats', $record);}
		elseif(in_array($sid, $node_res1))
			{
			db_update('nodeview_stats')
			-> fields(array('created'), time())
			-> condition('nid', $node->nid, '=');
			
			}
		elseif(in_array($sid, $node_res2) & ((time() - $res) > variable_get('nodeview_stats_interval')))
				{
				drupal_write_record('nodeview_stats', $record);
				}
		
	  }
    }
  }
}



/**
 * Helper function to get node_info from nodeview_stats_nodes.
 */
function _nodeview_stats_get_node_info($node) {
  if (isset($node->nid) && $node->nid) {
    $result = db_select('nodeview_stats_nodes', 'nvs')->fields('nvs')->condition('nid', $node->nid, '=')->execute();
    $data = $result->fetchAssoc();
    if (empty($data)) {
     return $data;
    }

    $data['view_mode'] = unserialize($data['view_mode']);
    return $data;
  }

  return array();
}

/**
 * Implements hook_ctools_render_alter().
 */
function nodeview_stats_ctools_render_alter(&$info, &$page, &$context) {
  $data = array_values($context['contexts']);
  // Below is nothing but to call hook_node_view which force to pass the
  // node object.
  module_invoke_all('node_view', $data[0]->data, 'full', '');
}

/**
 * Implements hook_views_api().
 */
function nodeview_stats_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_menu().
 */
function nodeview_stats_menu() {
  $items = array();

  $items['admin/config/people/nodeview_stats'] = array(
    'title' => 'The period for Nodeview Stats',
    'description' => 'Add the interval, after submit track',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodeview_stats_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/*
* page arguments for nodeview_stats_menu()
*/

function nodeview_stats_form($form, &$form_state) {

 $form['nodeview_stats_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('The interval'),
	'#default_value' => variable_get('nodeview_stats_interval', 20),
	'#size' => 3,
    '#maxlength' => 3,
    '#description' => t('The interval for checking session status in seconds.'),
    '#required' => TRUE,
  );
  
  $users = entity_load('user'); //users list

  $form['nodeview_not_tracking_users']=array(
  '#type' =>'checkboxes',
  '#title' => 'The users, that NOT included in NodeView stats',
  '#options' =>array_keys($users), //users id as checkbox option
  '#default_value' =>variable_get('nodeview_not_tracking_users'), // cannot exclude 0 non registered users;
  );

  return system_settings_form($form);
}


