<?php
/**
 * @file
 * Nodeview_stats.views.inc.
 */

/**
 * Implements hook_views_data().
 */
function nodeview_stats_views_data() {
  $data['nodeview_stats']['table']['group'] = t('Nodeview stats');
  $data['nodeview_stats']['table']['base'] = array(
    // Field to identify the view.
    'field' => 'id',
    'title' => t('Nodeview Stats'),
    'help' => t('Nodeview stats table contains statistics and can be related to nodes.'),
    'weight' => -10,
  );

  $data['nodeview_stats']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('Nodeview Stats node id.'),
    'relationship' => array(
      // The name of the table to join with.
      'base' => 'node',
      // The name of the field on the joined table.
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'title' => t('Node'),
    ),
  );

  $data['nodeview_stats']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('User who viewed node.'),
    'relationship' => array(
      // The name of the table to join with.
      'base' => 'users',
      // The name of the field on the joined table.
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'title' => t('User'),
    ),
  );

  // Example timestamp field.
  $data['nodeview_stats']['created'] = array(
    'title' => t('Created'),
    'help' => t('Nodeview Stats create timestamp.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['nodeview_stats']['sid'] = array(
    'title' => t('Sid'),
    'help' => t('Nodeview Stats session id.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
    $data['nodeview_stats']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('Nodeview Stats user id.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
      $data['nodeview_stats']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('Nodeview Stats node id.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );



  return $data;
}
